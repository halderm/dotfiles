# VScode Cheatsheet

## Global

* **Cmd-Shift P** :: Open command
* **Cmd p** :: Open file
* **Cmd-Shift F** :: Find in files
* **Ctrl `** :: toggle terminal

## Bookmarks

* **Leader m** :: Bookmark.toggle
* **Leader b** :: Bookmarks.list
* **Alt-Cmd j,l** :: Next, previous bookmark

## Navigation

* **Cmd 1,2** :: Switch to workspace
* **Ctrl 1,2** :: Switch to file in workspace direkt
* **Ctrl Tab** :: Switch to file in workspace dialog

## Vim

* **[Insert]vv** :: Esc
* **Leader jk** :: Save file
* **Leader jl** :: Save file and quit

## Easymotion

* **Leader Leader s char** :: Search char
* **Leader Leader w** :: Search word forward
* **Leader Leader b** :: Search word backward
* **Leader Leader j,k** :: Line forward, backward

## Surround and Commentary

* **d s char** :: Delete char
* **c s char newchar** :: Change existing char
* **y s motion char** :: Surround motion with char

  *Use open char to include spaces and close char without spaces*

* **[Visual]S char** :: Surround selection with char
* **gc motion** :: Toggle comment for motion

## OSX

* `sudo /usr/sbin/DevToolsSecurity -enable` :: enable dev mode
* **Ctrl-Alt i** Hotkey for iTerm2
